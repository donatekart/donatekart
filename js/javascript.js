(function ($, document, window) {

	$(document).ready(function () {

		var topMenuHandler = function () {
			var currentScrollPosition = $(window).scrollTop();
			if (0 == currentScrollPosition) {
				$("#header").removeClass("mini-menu");
			} else {
				$("#header").addClass("mini-menu");
			}
		}

		$(window).scroll(topMenuHandler);
		topMenuHandler();

		// Vertical centered modals
		// you can give custom class like this // var modalVerticalCenterClass = ".modal.modal-vcenter";
		var modalVerticalCenterClass = ".modal";

		function centerModals($element) {
			var $modals;
			if ($element.length) {
				$modals = $element;
			} else {
				$modals = $(modalVerticalCenterClass + ':visible');
			}
			$modals.each(function (i) {
				var $clone = $(this).clone().css('display', 'block').appendTo('body');
				var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
				top = top > 0 ? top : 0;
				$clone.remove();
				$(this).find('.modal-content').css("margin-top", top);
			});
		}
		$(modalVerticalCenterClass).on('show.bs.modal', function (e) {
			centerModals($(this));
		});
		$(window).on('resize', centerModals);


		function Donut_chart(options) {

			this.settings = $.extend({
				element: options.element,
				percent: 100
			}, options);


			this.circle = this.settings.element.find('path');
			this.settings.stroke_width = parseInt(this.circle.css('stroke-width'));
			this.radius = (parseInt(this.settings.element.css('width')) - this.settings.stroke_width) / 2;
			this.angle = -97.5; // Origin of the draw at the top of the circle
			this.i = Math.round(0.75 * this.settings.percent);
			this.first = true;

			this.animate = function () {
				this.timer = setInterval(this.loop.bind(this), 10);
			};

			this.loop = function (data) {
				this.angle += 5;
				this.angle %= 360;
				var radians = (this.angle / 180) * Math.PI;
				var x = this.radius + this.settings.stroke_width / 2 + Math.cos(radians) * this.radius;
				var y = this.radius + this.settings.stroke_width / 2 + Math.sin(radians) * this.radius;
				if (this.first == true) {
					var d = this.circle.attr('d') + " M " + x + " " + y;
					this.first = false;
				} else {
					var d = this.circle.attr('d') + " L " + x + " " + y;
				}
				this.circle.attr('d', d);
				this.i--;

				if (this.i <= 0) {
					clearInterval(this.timer);
				}
			}
		};

		$(function () {
			$('.donut-chart').each(function (index) {
				$(this).append('<svg preserveAspectRatio="xMidYMid" xmlns:xlink="http://www.w3.org/1999/xlink" id="donutChartSVG' + index + '"><path d="M100,100" /></svg>');
				var p = new Donut_chart({
					element: $('#donutChartSVG' + index),
					percent: $(this).attr('data-percent')
				});
				p.animate();
			});
		});

		$('a.js-link-scroll').on('shown.bs.tab', function (e) {
			$('#campaignTabs .active').removeClass('active');
			$('a[href="' + $(this).attr('href') + '"]').parent().addClass('active');
			var that = this;
			$('html, body').animate({
				scrollTop: $('#single-campaign-details').offset().top
			}, 500);
		});


		$('.single-campaign-details-wrap #campaignTabs a, .single-campaign-daan-utsav-details #campaignTabs a').on('click', function (e) {
			//$('#campaignTabs .active').removeClass('active');
			var redirectTo = $(this).attr('href');
			// $(this).parent().addClass('active');
			if ($(window).width() <= 767) {
				$('html, body').animate({
					scrollTop: $(redirectTo).offset().top - 130
				}, 500);
			} else {
				$('html, body').animate({
					scrollTop: $(redirectTo).offset().top - 75
				}, 500);
			}
		});

		$('body').on('click', '.redirect-to-promoters', function (e) {
			e.preventDefault();
			var redirectTo = $(this).attr('href');
			if ($(window).width() > 767) {
				$('html, body').animate({
					scrollTop: $(redirectTo).offset().top - 75
				}, 500);
			}
		});

		var campaignSocialHandler = function () {
			var currentScrollPosition = $(window).scrollTop();
			$("#single-campaign-social-wrapper-mobile").hide();
			if ($(window).width() <= 767) {
				if (120 < currentScrollPosition) {
					$("#single-campaign-social-wrapper-mobile").show();
				}
			}
		}

		$(window).scroll(campaignSocialHandler);
		campaignSocialHandler();


		// Cache selectors
		var lastId,

			campaignTabs = $("#campaignTabs"),
			campaignTabsHeight = campaignTabs.outerHeight() + 100,

			// All list items
			tabItems = campaignTabs.find("a"),
			// Anchors corresponding to menu items
			scrollItems = tabItems.map(function () {
				var item = $($(this).attr("href"));
				if (item.length) { return item; }
			});

		$(window).scroll(function () {
			// Get container scroll position
			var fromTop = $(this).scrollTop() + campaignTabsHeight;

			// Get id of current scroll item
			var cur = scrollItems.map(function () {
				if ($(this).offset().top < fromTop)
					return this;
			});
			// Get the id of the current element
			cur = cur[cur.length - 1];
			var id = cur && cur.length ? cur[0].id : "";
			if (!id.length) {
				tabItems.find(':first-child').addClass("active");
				return;
			}
			if (lastId !== id) {
				lastId = id;
				// Set/remove active class
				tabItems
					.parent().removeClass("active")
					.end().filter("[href='#" + id + "']").parent().addClass("active");
			}
		});

		/* $('a#updates-tab').on('click', function (e) {
			if ($(window).width() <= 767) {
				$('html, body').animate({
					scrollTop: $('#daan-utasv-updates').offset().top - 180
				}, 500);
			} else {
				$('html, body').animate({
					scrollTop: $('#daan-utasv-updates').offset().top - 120
				}, 500);
			}
		}); */

		//Scroll Menu Related code for Single Campaign
		var scrollMenu = $("#campaign-tabs-wrapper");
		var scrollMenuTabs = scrollMenu.find('#campaignTabs');
		var scrollMenuEndOffset = function () {
			var menuEndOffset = '';

			if ($("#how-dk-works").length) {
				menuEndOffset = $("#how-dk-works").offset().top - scrollMenuTabs.height();
			} else if ($("#testimonials").length) {
				menuEndOffset = $("#testimonials").offset().top - scrollMenuTabs.height();
			} else if ($("#footer").length) {
				menuEndOffset = $("#footer").offset().top - scrollMenuTabs.height();
			}

			return menuEndOffset;
		}
		// var scrollMenuEndOffset = $("#how-dk-works").offset().top - scrollMenuTabs.height();
		var stickySubMenuHandler = function () {
			var currentScrollPosition = $(window).scrollTop();
			if (($(window).width() <= 767) && scrollMenuTabs.length) {
				currentScrollPosition = $(window).scrollTop() + 72;
			}
			if (currentScrollPosition > scrollMenu.offset().top) {
				if (currentScrollPosition < scrollMenuEndOffset()) {
					if (scrollMenuTabs.css('position') != 'fixed') {
						scrollMenuTabs.css({ "position": "fixed", "transform": "translate3d(0px, 0px, 0px)" });
					}
					if (!$(".single-campaign").hasClass("stick-scroll-menu")) {
						$(".single-campaign").addClass("stick-scroll-menu");
					}
				} else {
					if (scrollMenuTabs.css('position') != 'relative') {
						scrollMenuTabs.css({ "position": "relative", "transform": "translate3d(0px, 0px, 0px)" });
					}
				}
			} else {
				if (scrollMenuTabs.css('position') != 'relative') {
					scrollMenuTabs.css({ "position": "relative", "transform": "translate3d(0px, 0px, 0px)" });
				}
				$(".single-campaign").removeClass("stick-scroll-menu");
			}
		}
		if (scrollMenu.length) {
			$(window).scroll(stickySubMenuHandler);
			stickySubMenuHandler();
		}

		// Scroll Product Related code for Single Campaign
		var productCardWidth = $('#productCard').innerWidth();
		var scrollProduct = $("#product-card-wrapper");
		var scrollProductCard = scrollProduct.find("#productCard");
		var bottomSectionOffset = function () {
			var bottomOffset = '';

			if ($("#how-dk-works").length) {
				bottomOffset = $("#how-dk-works").offset().top;
			} else if ($("#testimonials").length) {
				bottomOffset = $("#testimonials").offset().top;
			} else if ($("#footer").length) {
				bottomOffset = $("#footer").offset().top;
			}

			return bottomOffset;
		}
		var scrollEndOffset = bottomSectionOffset() - scrollProductCard.height();
		var fixedPosition = bottomSectionOffset() - scrollProductCard.height() - scrollProductCard.height() / 2;
		var stickyProductCardHandler = function () {
			var currentScrollPosition = $(window).scrollTop();
			if (currentScrollPosition > scrollProduct.offset().top) {
				if (currentScrollPosition < scrollEndOffset) {
					if (scrollProductCard.css('position') != 'fixed') {
						scrollProductCard.css({ "position": "fixed", "transform": "translate3d(0px, 0px, 0px)" });
					}
					if (!$(".single-campaign").hasClass("stick-product-card")) {
						$(".single-campaign").addClass("stick-product-card");
						scrollProductCard.width(productCardWidth);
					}
				} else {
					if (scrollProductCard.css('position') != 'relative') {
						scrollProductCard.css({ "position": "relative", "transform": "translate3d(0px, " + fixedPosition + "px, 0px)" });
					}
				}
			} else {
				if (scrollProductCard.css('position') != 'relative') {
					scrollProductCard.css({ "position": "relative", "transform": "translate3d(0px, 0px, 0px)" });
				}
				$(".single-campaign").removeClass("stick-product-card");
			}
		}
		if (scrollProduct.length) {
			$(window).scroll(stickyProductCardHandler);
			stickyProductCardHandler();
		}

		//Scroll Mobile Product Related code for Single Campaign
		if ($("#product-social-wrapper-mobile").length) {
			var scrollProductCardMobile = $("#product-social-wrapper-mobile");
			var stickyProductCardMobileHandler = function () {
				var currentScrollPosition = $(window).scrollTop();
				if (currentScrollPosition > scrollProductCardMobile.offset().top) {
					if (!$(".all-wrapper").hasClass("mobile-sticky-items")) {
						$(".all-wrapper").addClass("mobile-sticky-items");
					}
				} else {
					$(".all-wrapper").removeClass("mobile-sticky-items");
				}
			}
			if (($(window).width() <= 767) && scrollProductCardMobile.length) {
				$(window).scroll(stickyProductCardMobileHandler);
				stickyProductCardMobileHandler();
			}
		}

		//Scroll Link Wrapper Related code for Single Campaign
		var scrollHowWorks = $("#how-dk-works");
		var stickyLinkMobileHandler = function () {
			var currentScrollPosition = $(window).scrollTop();
			if (currentScrollPosition > scrollHowWorks.offset().top) {
				if (!$(".all-wrapper").hasClass("mobile-stick-link-wrapper")) {
					$(".all-wrapper").addClass("mobile-stick-link-wrapper");
				}
			} else {
				$(".all-wrapper").removeClass("mobile-stick-link-wrapper");
			}
		}
		if (($(window).width() <= 767) && scrollHowWorks.length) {
			$(window).scroll(stickyLinkMobileHandler);
			stickyLinkMobileHandler();
		}

		//Scroll Mobile Product Related code for Single Campaign
		if ($("#corona-campaign-btn-wrapper").length) {
			var scrollCoronaCampaignBtnWrapper = $("#corona-campaign-btn-wrapper");
			var stickyCoronaCampaignBtnHandler = function () {
				var currentScrollPosition = $(window).scrollTop();
				if (currentScrollPosition > scrollCoronaCampaignBtnWrapper.offset().top) {
					if (!$(".all-wrapper").hasClass("sticky-corona-btn-wrapper")) {
						$(".all-wrapper").addClass("sticky-corona-btn-wrapper");
					}
				} else {
					$(".all-wrapper").removeClass("sticky-corona-btn-wrapper");
				}
			}
			$(window).scroll(stickyCoronaCampaignBtnHandler);
			stickyCoronaCampaignBtnHandler();
		}

		/*var toggleProductsButton = $('.view-more-campaign-products');
		var toggleCampaignProducts = function () {
			var productsCardWrapper = toggleProductsButton.parents('#products-tab-panel').find('.product-cards-wrapper');
			
			toggleProductsButton.html('See More');
			productsCardWrapper.toggleClass('campaign-seemore-wrapper');
			toggleProductsButton.parents('#products-tab-panel').find('.more-campaign-products').slideToggle();

			if(!productsCardWrapper.hasClass('campaign-seemore-wrapper')){
				toggleProductsButton.html('See Less');
			}
		}
		if (toggleProductsButton.length) {
			$(toggleProductsButton).on('click', function (e) {
				e.preventDefault();
				toggleCampaignProducts();
			});
		}*/

		var toggleProductButton = $('.view-more-campaign-products');
		var productWrapper = toggleProductButton.parents('#products-tab-panel').find('.product-cards-wrapper');
		var productContentWrap = productWrapper.find('.campaign-product-content-wrapper');
		var productContentWrapHeight = productContentWrap.height();
		var toggleCampaignProducts = function () {
			if (productWrapper.hasClass("campaign-seemore-wrapper")) {
				productWrapper.removeClass('campaign-seemore-wrapper');
			}

			productContentWrap.toggleClass('more-campaign-product-height');

			if (productContentWrap.hasClass("more-campaign-product-height")) {
				productWrapper.addClass('campaign-seemore-wrapper');

				if ($(window).width() <= 1199) {
					productContentWrap.animate({ "height": "2100px" });
				} else {
					productContentWrap.animate({ "height": "900px" });
				}

				toggleProductButton.html('See More');
			} else {
				productWrapper.removeClass('campaign-seemore-wrapper');
				productContentWrap.animate({ "height": productContentWrapHeight });
				toggleProductButton.html('See Less');
			}
		}
		if (toggleProductButton.length) {
			$(toggleProductButton).on('click', function (e) {
				e.preventDefault();
				toggleCampaignProducts();
			});
			toggleCampaignProducts();
		}

		var toggleAboutButton = $('.view-more-campaign-about');
		var aboutWrapper = toggleAboutButton.parents('#about-campaign').find('.campaign-about-wrapper');
		var aboutContentWrap = aboutWrapper.find('.campaign-about-content-wrapper');
		var aboutContentWrapHeight = aboutContentWrap.height();
		var toggleAboutCampaign = function () {
			if (aboutWrapper.hasClass("campaign-seemore-wrapper")) {
				aboutWrapper.removeClass('campaign-seemore-wrapper');
			}

			aboutContentWrap.toggleClass('more-campaign-about-height');

			if (aboutContentWrap.hasClass("more-campaign-about-height")) {
				aboutWrapper.addClass('campaign-seemore-wrapper');
				aboutContentWrap.animate({ "height": "500px" });
				toggleAboutButton.html('See More');
			} else {
				aboutWrapper.removeClass('campaign-seemore-wrapper');
				aboutContentWrap.animate({ "height": aboutContentWrapHeight });
				toggleAboutButton.html('See Less');
			}
		}
		if (toggleAboutButton.length) {
			$(toggleAboutButton).on('click', function (e) {
				e.preventDefault();
				toggleAboutCampaign();
			});
			toggleAboutCampaign();
		}

		var toggleUpdatesButton = $('.view-more-campaign-updates');
		var toggleUpdates = function () {
			if ($("#daan-utasv-updates").length) {
				var daanUtsavUpdatesWrapper = toggleUpdatesButton.parents('#daan-utasv-updates').find('.daan-utasv-updates-wrapper');

				toggleUpdatesButton.html('See More');
				daanUtsavUpdatesWrapper.toggleClass('campaign-seemore-wrapper');
				toggleUpdatesButton.parents('#daan-utasv-updates').find('.more-campaign-updates').slideToggle();

				if (!daanUtsavUpdatesWrapper.hasClass('campaign-seemore-wrapper')) {
					toggleUpdatesButton.html('See Less');
				}
			} else if ($("#campaign-updates").length) {
				var campaignUpdatesWrapper = toggleUpdatesButton.parents('#campaign-updates').find('.campaign-updates-wrapper');

				toggleUpdatesButton.html('See More');
				campaignUpdatesWrapper.toggleClass('campaign-seemore-wrapper');
				toggleUpdatesButton.parents('#campaign-updates').find('.more-campaign-updates').slideToggle();

				if (!campaignUpdatesWrapper.hasClass('campaign-seemore-wrapper')) {
					toggleUpdatesButton.html('See Less');
				}
			}
		}
		if (toggleUpdatesButton.length) {
			$(toggleUpdatesButton).on('click', function (e) {
				e.preventDefault();
				toggleUpdates();
			});
		}

		var toggleSidebarAboutButton = $('.view-more-about');
		var sidebarAboutWrapper = toggleSidebarAboutButton.parents('.about-campaigner').find('.about-wrapper');
		var sidebarAboutContentWrap = sidebarAboutWrapper.find('.about-content-wrapper');
		var sidebarAboutContentWrapHeight = sidebarAboutContentWrap.height();
		var toggleSidebarAbout = function () {
			if (sidebarAboutWrapper.hasClass("seemore-wrapper")) {
				sidebarAboutWrapper.removeClass('seemore-wrapper');
			}

			sidebarAboutContentWrap.toggleClass('more-about-height');

			if (sidebarAboutContentWrap.hasClass("more-about-height")) {
				sidebarAboutWrapper.addClass('seemore-wrapper');
				sidebarAboutContentWrap.animate({ "height": "200px" });
				toggleSidebarAboutButton.html('See More');
			} else {
				sidebarAboutWrapper.removeClass('seemore-wrapper');
				sidebarAboutContentWrap.animate({ "height": sidebarAboutContentWrapHeight });
				toggleSidebarAboutButton.html('See Less');
			}
		}
		if (toggleSidebarAboutButton.length) {
			$(toggleSidebarAboutButton).on('click', function (e) {
				e.preventDefault();
				toggleSidebarAbout();
			});
			toggleSidebarAbout();
		}

		$('.donate-tooltip').tooltip({ placement: 'top', trigger: 'manual' }).tooltip('show');
		$linkBtnClicked = false;
		$('body').on('mouseenter', '.donate-link-tooltip', function (e) {
			e.preventDefault();
			if ($linkBtnClicked != true) {
				$(this).tooltip({ placement: 'top', trigger: 'manual' }).tooltip('show');
			}
		});

		$('body').on('mouseleave', '.donate-link-tooltip', function (e) {
			e.preventDefault();
			if ($linkBtnClicked != true) {
				$(this).tooltip('destroy');
			}
		});

		$('body').on('click', '.donate-link-tooltip', function (e) {
			e.preventDefault();
			//$(this).tooltip('hide');
			$linkBtnClicked = true;
			if ($linkBtnClicked == true) {
				var obj = $(this);
				$(this).attr({
					'title': 'Link Copied',
					'data-original-title': 'Link Copied'
				}).tooltip({ placement: 'top', trigger: 'manual' }).tooltip('show');
				// $(this).parent().find('.tooltip-inner').html('Link Copied');
				// $(this).tooltip({placement: 'top',trigger: 'manual'}).tooltip('show');
				setTimeout(function () {
					obj.tooltip('destroy');
					obj.attr({
						'title': 'Copy Campaign Link',
						'data-original-title': 'Copy Campaign Link'
					});
					obj.tooltip({ placement: 'top', trigger: 'manual' });
					$linkBtnClicked = false;
				}, 3000);
			}

		});

		/* Start: Related to Show more campaigns button working. */
		$(".campaigns-and-btn-wrapper .campaigns-wrapper .more-campaign").slice(0, 0).show();
		if ($(".campaigns-and-btn-wrapper .campaigns-wrapper .campaign-card:hidden").length != 0) {
			$(".campaigns-and-btn-wrapper .show-more-campaign").show();
		}

		$("body").on('click', '.campaigns-and-btn-wrapper .show-more-campaigns', function (e) {
			e.preventDefault();
			$(this).parents(".campaigns-and-btn-wrapper").find(".campaigns-wrapper .more-campaign:hidden").slice(0, 12).slideDown().css("display", "inline-block");
			if ($(".more-campaign:hidden").length == 0) {
				$(this).fadeOut("slow");
				/* $(this).html('Show Less').addClass('show-less-campaigns').removeClass('show-more-campaigns'); */
			}
		});

		/* $("body").on('click', '.campaigns-and-btn-wrapper .show-less-campaigns', function (e) {
			e.preventDefault();
			$(".more-campaign").slideUp("slow");
			$(this).html('Show More').addClass('show-more-campaigns').removeClass('show-less-campaigns');
		}); */
		/* End: Related to Show more campaigns button working. */



		var scrollHandler = function () {
			//Show/hide scroll to top
			($(this).scrollTop() > 320) ?
				$('.scroll-to-top').fadeIn() :
				$('.scroll-to-top').fadeOut();
		}
		$(window).scroll(200, scrollHandler);
		scrollHandler();

		$("body").on('click', '.scroll-to-top', function (e) {
			e.preventDefault();
			$("html, body").animate({ scrollTop: $('body').offset().top - 70 }, 1000);
			return false;
		});

		$("body").on('click', '.edit-content-wrap .action-btns-wrapper .btn-edit', function (e) {
			e.preventDefault();
			$(this).parents(".edit-content-wrap").find(".display-content").hide();
			$(this).parents(".edit-content-wrap").find(".edit-content").show();
			$(this).parents(".campaign-about-wrapper").removeClass("campaign-seemore-wrapper");
			$(this).hide();
			$(this).parents(".edit-content-wrap").find(".action-btns-wrapper .btn-save").show();
		});

		$("body").on('click', '.edit-content-wrap .action-btns-wrapper .btn-save', function (e) {
			e.preventDefault();
			$(this).parents(".edit-content-wrap").find(".edit-content").hide();
			$(this).parents(".edit-content-wrap").find(".display-content").show();
			$(this).parents(".campaign-about-wrapper").addClass("campaign-seemore-wrapper");
			$(this).hide();
			$(this).parents(".edit-content-wrap").find(".action-btns-wrapper .btn-edit").show();
		});

	});

	$('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
	$('body').find('.quantity').each(function () {
		var spinner = jQuery(this),
			input = spinner.find('input[type="number"]'),
			btnUp = spinner.find('.quantity-up'),
			btnDown = spinner.find('.quantity-down'),
			min = input.attr('min'),
			max = input.attr('max');

		btnUp.click(function () {
			var oldValue = parseFloat(input.val());
			if (oldValue >= max) {
				var newVal = oldValue;
			} else {
				var newVal = oldValue + 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("change");
		});

		btnDown.click(function () {
			var oldValue = parseFloat(input.val());
			if (oldValue <= min) {
				var newVal = oldValue;
			} else {
				var newVal = oldValue - 1;
			}
			spinner.find("input").val(newVal);
			spinner.find("input").trigger("change");
		});

	});

	$('.is-matching .product-sponsor-info .info-btn').popover({
		title: '',
		content: '<div class="tooltip-content-wrapper"><div class="row"><div class="col-xs-3 tooltip-image-content"><div class="tooltip-image-wrapper"><img src="images/cart-icon.svg" /></div><p>For Every Product you donate to this campaign</p></div><div class="col-xs-1 tooltip-sign-content"><h2>+</h2></div><div class="col-xs-3 tooltip-image-content"><div class="tooltip-image-wrapper"><img src="images/cart-icon.svg" /></div><p>Our Matching Partner will add one more product to match your contribution</p></div><div class="col-xs-1 tooltip-sign-content"><h2>=</h2></div><div class="col-xs-4 tooltip-image-content"><div class="tooltip-image-wrapper"><img src="images/truck-icon.svg" /></div><p>Multiplying your impact toward this cause</p></div></div></div>',
		template: '<div class="popover dk-info-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><button type="button" class="close" data-dismiss="popover" aria-hidden="true"><i class="zmdi zmdi-close-circle-o"></i></button><div class="popover-content"></div></div>',
		html: true,
		animation: true,
		placement: 'top'
	});

	$('.is-matching-campaign .matching-cmp .info-btn').popover({
		title: '',
		template: '<div class="popover dk-info-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><button type="button" class="close" data-dismiss="popover" aria-hidden="true"><i class="zmdi zmdi-close-circle-o"></i></button><div class="popover-content"></div></div>',
		animation: true,
		placement: 'top'
	});

	$('.has-tax-sticker .tax-benefit').tooltip({
		title: '',
		template: '<div class="tooltip dk-info-tooltip dk-tax-benefit-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
		animation: true,
		placement: 'top',
		container: 'body',
		html: true,
	});

	$('body').on('click', '.popover .close', function (e) {
		e.preventDefault();
		(($(this).parents(".popover").popover('hide').data('bs.popover') || {}).inState || {}).click = false // fix for BS 3.3.6
	});

	$(document).on('click', function (e) {
		$('[data-toggle="popover"],[data-original-title]').each(function () {
			//the 'is' for buttons that trigger popups
			//the 'has' for icons within a button that triggers a popup
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
				(($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false // fix for BS 3.3.6
			}
		});
	});

	$(document).ready(function () {
		$('.donatekart-campaign-carousel').owlCarousel({
			responsiveClass: true,
			autoplay: true,
			autoplaySpeed: 500,
			autoplayHoverPause: true,
			lazyLoad: true,
			loop: true,
			responsive: {
				0: {
					items: 1,
					nav: false,
					dots: true,
				},
				1300: {
					items: 3,
					nav: true,
					dots: false,
				}
			}
		});

		$('.donatekart-video-carousel').owlCarousel({
			responsiveClass: true,
			autoplaySpeed: 500,
			autoplayHoverPause: true,
			lazyLoad: true,
			responsive: {
				0: {
					items: 1,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
				},
				1300: {
					items: 3,
					nav: true,
					dots: false,
				}
			}
		});

		$('.donatekart-featured-carousel').owlCarousel({
			responsiveClass: true,
			autoplaySpeed: 500,
			autoplayHoverPause: true,
			lazyLoad: true,
			loop: true,
			responsive: {
				0: {
					items: 1,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
				},
				580: {
					items: 3,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
				},
				1300: {
					items: 6,
					nav: true,
					dots: false,
				}
			}
		});

		$('.why-donatekart-wrap-carousel').owlCarousel({
			responsiveClass: true,
			autoplay: 3000,
			autoplayHoverPause: true,
			lazyLoad: true,
			items: 1,
			nav: false,
			dots: true,
			loop: true,
		});

		$('.donatekart-featured-carouselv2').owlCarousel({
			responsiveClass: true,
			autoplaySpeed: 500,
			autoplayHoverPause: true,
			lazyLoad: true,
			loop: true,
			responsive: {
				0: {
					items: 2,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
					center: true
				},
				580: {
					items: 3,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
				},
				1300: {
					items: 6,
					nav: true,
					dots: false,
				}
			}
		});

		$('.donatekart-supported-carousel').owlCarousel({
			responsiveClass: true,
			autoplaySpeed: 500,
			autoplayHoverPause: true,
			lazyLoad: true,
			loop: true,
			responsive: {
				0: {
					items: 1,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,

				},
				580: {
					items: 3,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
				},
				1300: {
					items: 3,
					nav: true,
					dots: false,
				}
			}
		});

		$('.donatekart-supported-carouselv2').owlCarousel({
			responsiveClass: true,
			autoplaySpeed: 500,
			autoplayHoverPause: true,
			lazyLoad: true,
			loop: true,
			responsive: {
				0: {
					items: 2,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
					center: true

				},
				580: {
					items: 3,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
				},
				1300: {
					items: 3,
					nav: true,
					dots: false,
				}
			}
		});

		$('.three-carousel-items').owlCarousel({
			responsiveClass: true,
			autoplaySpeed: 500,
			autoplayHoverPause: true,
			lazyLoad: true,
			responsive: {
				0: {
					items: 1,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
				},
				1300: {
					items: 3,
					nav: true,
					dots: true,
					nav: false,
				}
			}
		});

		$('.four-carousel-items').owlCarousel({
			responsiveClass: true,
			autoplaySpeed: 500,
			autoplayHoverPause: true,
			lazyLoad: true,
			responsive: {
				0: {
					items: 1,
					nav: false,
					dots: true,
					loop: true,
					autoplay: true,
				},
				1300: {
					items: 4,
					nav: true,
					dots: true,
				}
			}
		});

		//Animated Map using SVG
		if ($("#dk-india-map-wrapper").length) {
			var s = Snap("#dk-india-map-wrapper");

			var svgHandler = function (svg) {
				s.append(svg);
				var statePaths = s.selectAll('.state-path.has-content');
				var counter = 0;

				var animator = function () {
					var textSelector = '#' + statePaths[counter].attr("id") + '-text';
					statePaths[counter].removeClass('animate_state');
					// console.log(textSelector);
					s.select(textSelector).removeClass('animate_state');
					if (counter === (statePaths.length - 1)) {
						counter = 0;
					} else {
						counter += 1;
					}
					var textSelector = '#' + statePaths[counter].attr("id") + '-text';
					statePaths[counter].addClass('animate_state');
					s.select(textSelector).addClass('animate_state');
				}

				$.each(statePaths, function (index, statePath) {
					statePath.hover(function () {
						var textSelector = '#' + this.attr("id") + '-text';
						this.addClass('animate_state');
						s.select(textSelector).addClass('animate_state');
					}, function () {
						var textSelector = '#' + this.attr("id") + '-text';
						this.removeClass('animate_state');
						s.select(textSelector).removeClass('animate_state');
					})
				});


				// Start automatic animation
				animator();
				setInterval(function () {
					animator();
				}, 3000);
			}
			Snap.load('images/india-map.svg', svgHandler);
		}


	});


	$(".datepicker").each(function (index) {
		$(this).datetimepicker({
			format: 'YYYY/MM/DD',
			allowInputToggle: true

		});
	});


})(jQuery, document, window);